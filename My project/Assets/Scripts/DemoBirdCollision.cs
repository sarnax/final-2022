using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BirdDemoCollision : MonoBehaviour
{
    public GameObject deathExplosion;
    public GameObject checkpointSound;
    public Text scoreText;
    public int score;

    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
    if (other.gameObject.tag == "Environment")
        {
           GameObject deathObj = Instantiate(deathExplosion, transform.position, transform.rotation) as GameObject;
           Destroy(gameObject);
        }
        else if (other.gameObject.tag == "Checkpoint")
        {
          score += 1;
          scoreText.text = "SCORE : " + score.ToString();
          Instantiate(checkpointSound, transform.position, transform.rotation);
        }
    }
}