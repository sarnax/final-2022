using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdDemoObstacle : MonoBehaviour
{
    public float sendTimer = 1;
    public float frequency = 2;
    public float position;
    public GameObject myObstacle;
    public GameObject mainCharacter;

    void Update()
    {
        sendTimer -= Time.deltaTime;
        if (sendTimer <= 0)
        {
            position = Random.Range(.68f, 2f);
            transform.position = new Vector3(0, position, -3.7f);
            Instantiate(myObstacle, transform.position, transform.rotation);
            sendTimer = frequency;
        }

        if (mainCharacter != null) Time.timeScale = 1;
        else Time.timeScale = 0;
    }
}
